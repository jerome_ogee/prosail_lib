; PROCEDURE COMMENTS +
; NAME: campbell
; AUTHOR: Translated from Matlab to IDL by Eben N. Broadbent 
;  Dept. of Biology, Stanford University.
;  
; Original Matlab code written by: Jean-Baptiste Féret (feret@ipgp.fr)
; Institut de Physique du Globe de Paris, Space and Planetary Geophysics
; during October 2009 based on a version of PROSAIL provided by
; Wout Verhoef, NLR on April/May 2003.
;  
; CONTACT INFO: ebennb@gmail.com
; DESCRIPTION:
; CALLING SEQUENCE:
;       output = campbell(ala)
; INPUTS: ala
; OUTPUTS: output
; OPTIONAL OUTPUTS:
; OPTIONAL INPUT KEYWORD(S):
; NOTES:
; METHOD:
; EXAMPLE:
; MODIFICATION HISTORY:
;       Downloaded from "http://teledetection.ipgp.jussieu.fr/prosail/" on 09/10/2010
;       Translated from matlab to IDL by: Eben N. Broadbent on '09/27/2010'
;       Variable na and some checks added by: J. Ogee on '26/02/2013'
; CODING NOTES:
; Same as Matlab (09/27/2010)
;
;********************************************************************************
;*                          Campbell.f                            
;*     
;*    Computation of the leaf angle distribution function value (freq) 
;*    Ellipsoidal distribution function caracterised by the average leaf 
;*    inclination angle in degree (ala)                                     
;*    Campbell 1986                                                      
;*                                                                              
;********************************************************************************
;
;--- test to check the gama-xsi relationships
;--- g1 = approx. from Campbell (1990)
;--- g2 = approx. from Campbell (1986)
;--- g3 = exact value
;--- conclusions: all 3 formula agree well
;    BUT JB Feret seems to use a different formula (i.e. never seems
;--- to use epsi2 terms when xsi>1)
;xsi=findgen(100)/10
;g1=xsi+1.744*(xsi+1.182)^(-0.733)
;g2=1.47+0.45*xsi+0.1223*xsi^2-0.0130*xsi^3+0.000509*xsi^4
;g3=fltarr(100)
;ii=where(xsi lt 1, complement=jj)
;njj = n_elements(jj)
;epsi1=sqrt(1.-xsi[ii]^2)
;g3[ii] = xsi[ii] + asin(epsi1)/epsi1
;g3[jj[0]] = 2.
;epsi2=sqrt(1.-1./xsi[jj[1:njj-1]]^2)
;g3[jj[1:njj-1]] = xsi[jj[1:njj-1]] + alog((1+epsi2)/(1-epsi2))/(2.*epsi2*xsi[jj[1:njj-1]])
;plot,xsi,g1
;oplot,xsi,g2,linestyle=1
;oplot,xsi,g3,linestyle=2

;--- test to check the xsi-ala relationship
;--- xsi1 = approx. from Campbell (1990)
;--- xsi2 = approx. from JB Feret
;--- conclusions= JB Feret is far from Campbell for ala<5degrees (i.e. xsi >15)
;ala=findgen(90)+0.5
;alar=ala*!pi/180.
;xsi1 = (alar/9.65)^(-1./1.65) - 3.
;xsi2 = exp(-1.6184e-5*ala^3.0+2.1145e-3*ala^2.0-1.2390e-1*ala+3.2491)
;plot,ala,xsi1,thick=3
;oplot,xsi2,linestyle=2
;
;-
function campbell, na, ala
  CASE na OF
     13:BEGIN
        tx2=[0, 10, 20, 30, 40, 50, 60, 70, 80, 82, 84, 86, 88]
        tx1=[10, 20, 30, 40, 50, 60, 70, 80, 82, 84, 86, 88, 90]
     END
     18: BEGIN
        tx2=[0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85]
        tx1=[5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90]
     END
     ELSE: BEGIN
        tx2 = linspace(0           ,90*(1-1/FLOAT(na)), na)
        tx1 = linspace(90/FLOAT(na),90                , na)
     END
  ENDCASE
  ;---
  excent=exp(-1.6184e-5*ala^3.0+2.1145e-3*ala^2.0-1.2390e-1*ala+3.2491) ;--- Eben N. Broadbent says "value currently wrong"?!
  ;excent = (ala*!pi/180./9.65)^(-1./1.65) - 3. ;--- creates NaNs (for UKHam ADFC 2010 at least) because infinite at ala=0
  ;---
  tl1=tx1*(!pi/180.0)
  tl2=tx2*(!pi/180.0)
  sum0 = 0
  freq=fltarr(na)
  for i=0, na-1 do begin
     if (excent eq 1) then begin
        freq[i] = abs(cos(tl1[i])-cos(tl2[i]))
     endif else begin
        excent2 = excent*excent
        x1  = excent / (sqrt(1.0 + excent2 * tan(tl1[i])^2.0))
        x2  = excent / (sqrt(1.0 + excent2 * tan(tl2[i])^2.0))
        alpha  = excent / sqrt(abs(1.0 - excent2))
        alpha2 = alpha*alpha
        x12 = x1*x1
        x22 = x2*x2
        if excent gt 1 then begin
           alpx1 = sqrt(alpha2 + x12)
           alpx2 = sqrt(alpha2 + x22)
           dum   = x1*alpx1 + alpha2*alog(x1 + alpx1)
           freq[i] = abs(dum - (x2*alpx2 + alpha2*alog(x2 + alpx2)))
        endif else begin
           almx1 = sqrt(alpha2 - x12)
           almx2 = sqrt(alpha2 - x22)
           dum   = x1*almx1 + alpha2*asin(x1/alpha)
           freq[i] = abs(dum-(x2*almx2+alpha2*asin(x2/alpha)))
        endelse
     endelse
  endfor
  sum0 = total(freq)
  freq0=freq/sum0
  ;
  ;--- test ot check JB Feret integration
  ;xsi = excent
  ;if xsi lt 1 then begin
  ;   epsi = sqrt(1.-xsi^2)
  ;   gama = xsi + asin(epsi)/epsi
  ;endif else if xsi eq 1 then begin
  ;   gama = 2.
  ;endif else begin
  ;   epsi = sqrt(1.-1./xsi^2)
  ;   gama = xsi + alog((1+epsi)/(1-epsi)) / (2.*epsi*xsi)
  ;endelse
  ;fac = 2.*xsi^3/gama
  ;f_true = FLTARR(na)
  ;nint = 21
  ;FOR i=0,na-1 DO BEGIN
  ;   xarr = (tx2[i] + (tx1[i] -tx2[i])/float(nint-1)*findgen(nint)) * !pi/180.0
  ;   yarr = sin(xarr) / (cos(xarr)^2 + xsi^2*sin(xarr)^2)^2
  ;   sum = 0. 
  ;   FOR k=1,nint-1 DO BEGIN
  ;      sum = sum +  0.5 * ( yarr[k] + yarr[k-1] ) * ( xarr[k] - xarr[k-1] )
  ;   ENDFOR
  ;   f_true[i] = fac * sum
  ;   print,FORMAT='(i3,2f9.4)',i,freq0[i],f_true[i]
  ;ENDFOR
  ;plot,findgen(na),f_true,psym=-6,thick=5
  ;oplot,findgen(na),freq0,linestyle=2,color=200,thick=3
  ;print,'normalisation',sum0,2.*xsi^3/gama
  ;
return, freq0
end
