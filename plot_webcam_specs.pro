PRO plot_webcam_specs, path_for_plot=path_for_plot, $
                       camera_type=camera_type, $
                       RGB_color_tab=RGB_color_tab,$
                       l_UV_filter=l_UV_filter,$
                       l_IR_filter=l_IR_filter,$
                       l_quantum_yield=l_quantum_yield,$
                       quantum_yield=quantum_yield, $
                       l_inter = l_inter, $
                       qy_inter = qy_inter
                       
  @ogee_general_header.inc

  ;--- i/o stuff
  IF NOT(KEYWORD_SET(path_for_plot)) THEN path_for_plot = '' ;--- path for plots
  IF NOT(KEYWORD_SET(RGB_color_TAB)) THEN RGB_color_TAB =  [2, 63, 23]

  ;--- open ps file
  aspect_ratio=1.1
  xsize=8
  ysize=xsize/aspect_ratio
  set_plot, 'ps'
;  !p.font=0
  file = path_for_plot+'camera_specs_'+camera_type+'.ps'
  device, filename=file, encapsulated=1, /helvetica, /color
  device, xsize=xsize, ysize=ysize

  ;--- draw a frame
  !p.multi = 0
  plot,[0],[0],$
       title=stdfont+'Camera type '+camera_type,$
       charsize=0.8,$
       xthick=axis_thickness, ythick=axis_thickness, $
       xminor=1, yminor=1,$
       xrange=[MIN(l_quantum_yield)<l_UV_filter,MAX(l_quantum_yield)>l_IR_filter], $
       yrange=[0,MAX(CEIL(quantum_yield[*,*]*5.)/5.)], $ ;100], $
       xstyle=0, ystyle=0, $
       xtitle=stdfont+'Wavelength [nm]', ytitle=stdfont+'Relative intensity [%]', $
       font=!p.font, $
       /nodata, $
       color=0

  ;--- plot curves
  mc_symbol,sym=2,fill=0
  FOR ic=0,2 DO oplot,l_quantum_yield, quantum_yield[*,ic], color= RGB_color_TAB[ic], $
                      psym=8,symsize=0.5
  IF NOT(KEYWORD_SET(l_inter)) OR NOT(KEYWORD_SET(qy_inter)) THEN BEGIN
     l_inter  = dindgen(!x.crange[1]-!x.crange[0]) + !x.crange[0]
     FOR ic=0,2 DO BEGIN
        qy_inter = INTERPOL(REFORM(quantum_yield[*,ic]), l_quantum_yield, l_inter, /SPLINE) > 0.
        oplot,l_inter, qy_inter, $
              color= RGB_color_TAB[ic], thick=1, linestyle=2
     ENDFOR
  ENDIF ELSE BEGIN
     FOR ic=0,2 DO oplot,l_inter, qy_inter[*,ic], $
                         color= RGB_color_TAB[ic], thick=3
  ENDELSE

  oplot,[l_IR_filter,l_IR_filter], [!y.crange[0],!y.crange[1]]
  polyfill,[l_IR_filter,l_IR_filter,!x.crange[1],!x.crange[1]], $
           [!y.crange[0],!y.crange[1],!y.crange[1],!y.crange[0]], $
           orientation=45;, fill=0
  oplot,[l_UV_filter,l_UV_filter], [!y.crange[0],!y.crange[1]]
  polyfill,[l_UV_filter,l_UV_filter,!x.crange[0],!x.crange[0]], $
           [!y.crange[0],!y.crange[1],!y.crange[1],!y.crange[0]], $
           orientation=45;, fill=0
  
  ;--- close PS file
  device, /close
  set_plot, 'x'
  ;set_plot, 'win'
;  !p.font=-1

  ;--- converts to PDF and deletes PS file (for Mac only)
  IF (os NE 'Windows') THEN BEGIN
     cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
     ;cmd = 'ps2pdf '+file+' '+(STR_SEP(file,'.ps'))[0]+'.pdf'
     spawn,cmd
     FILE_DELETE, file
  ENDIF

END
