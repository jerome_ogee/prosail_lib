; PROCEDURE COMMENTS +
; NAME: main_PROSAIL_5B
; AUTHOR: Translated from Matlab to IDL by Eben N. Broadbent 
;  Dept. of Biology, Stanford University.
;  
; Original Matlab code written by: Jean-Baptiste Féret (feret@ipgp.fr)
; Institut de Physique du Globe de Paris, Space and Planetary Geophysics
; during October 2009 based on a version of PROSAIL provided by
; Wout Verhoef, NLR on April/May 2003.
; 
; CONTACT INFO: ebennb@gmail.com
; DESCRIPTION:
; CALLING SEQUENCE:
;       main_PROSAIL_5B
; INPUTS: none, parameters are set within the procedure
; OUTPUTS: resh and resv (hemi and directional reflectance values)
; OPTIONAL OUTPUTS:
; OPTIONAL INPUT KEYWORD(S):
; NOTES:
; METHOD:
; EXAMPLE:
; MODIFICATION HISTORY:
;       Downloaded from "http://teledetection.ipgp.jussieu.fr/prosail/" on 09/10/2010
;       Translated from matlab to IDL by: Eben N. Broadbent on '09/27/2010'
; CODING NOTES:
; Same as Matlab (09/24/2010)
; 
; !*************************************************************************
; !*                                                                       *
;   main_PROSAIL
; 
;   ! 10 20 2009
;   ! This program allows modeling reflectance data from canopy
;   ! - modeling leaf optical properties with PROSPECT-5 (feret et al. 2008)
;   ! - modeling leaf inclination distribution function with Campbell.f
;   ! (Ellipsoidal distribution function caracterised by the average leaf 
;   ! inclination angle in degree)
;   ! - modeling canopy reflectance with 4SAIL (Verhoef et al., 2007)
; 
;   ! Verhoef et al. (2007) Unified Optical-Thermal Four-Stream Radiative
;   ! Transfer Theory for Homogeneous Vegetation Canopies, IEEE TRANSACTIONS 
;   ! ON GEOSCIENCE AND REMOTE SENSING, VOL. 45, NO. 6, JUNE 2007
;   ! Féret et al. (2008), PROSPECT-4 and 5: Advances in the Leaf Optical
;   ! Properties Model Separating Photosynthetic Pigments, REMOTE SENSING OF 
;   ! ENVIRONMENT
;   ! The specific absorption coefficient corresponding to brown pigment is
;   ! provided by Frederic Baret (EMMAH, INRA Avignon, baret@avignon.inra.fr)
;   ! and used with his autorization.
;   ! the model PRO4SAIL is based on a version provided by
;   ! Wout Verhoef
;   ! NLR 
;   ! April/May 2003
;   
;   Downloaded from "http://teledetection.ipgp.jussieu.fr/prosail/" 09/15/2010
;    in Matlab format.
; 
; !*                                                                       *
; !*************************************************************************
; Initial leaf parameter values from:
; 1. Asner & Martin. 2009. Leaf Chemical and Optical Properties of Metrosideros polymorpha Across
;  Environmental Gradients in Hawaii. Biotropica.
;
;-

pro main_PROSAIL_5B

 ; EXAMPLE INPUT VALUES
 Cab    = 50.0d    ;; chlorophyll content (µg.cm-2) (20-80 possible for Ohia)
 Car    = 12.0d    ;; carotenoid content (µg.cm-2)  (5-18 possible for Ohia)
 Cbrown = 0.5d     ;; brown pigment content (arbitrary units) - wood/shade?
 Cw     = 0.015d   ;; EWT (cm) - Equivalent water thickness
 Cm     = 0.1d     ;; LMA (g.cm-2) - leaf mass per unit leaf area (0.05-0.2 possible for Ohia)
 Ns     = 1.0d     ;; structure coefficient
 angl   = 50.0d    ;; average leaf angle (°)
 lai    = 5.0d     ;; leaf area index
 hspot  = 0.2d     ;; hot spot
 tts    = 65.0d    ;; solar zenith angle (°)
 tto    = 0.0d     ;; observer zenith angle (°)
 psi    = 0.0d     ;; azimuth (°)
 psoil  = 1.0d     ;; soil coefficient
 skyl   = 70.0d    ;; diffuse/direct radiation

; PROSAIL output
; resh : hemispherical reflectance
; resv : directional reflectance

 PRO4SAIL5B,Cab,Car,Cbrown,Cw,Cm,Ns,angl,lai,hspot,tts,tto,psi,psoil,skyl,resh,resv;
  
 l=transpose(linspace(400,2500,2101));
 ref_out = [l,resh,resv]
 print, ref_out[*,500]
 plot,resv
 
 ;write_csv
 ;dlmwrite('Refl_CAN.txt',[l,resh,resv],'delimiter','\t','precision',5) ; NEED TO MAKE THIS OUTPUT WORKING

end
