PRO plot_solar_spectrum, path_for_plot=path_for_plot, $
                         skyl=skyl, Rg=Rg

  @ogee_general_header.inc
;s_micro = '!4l!3'
;stdfont='!3'

  ;--- i/o stuff
  IF NOT(KEYWORD_SET(path_for_plot)) THEN path_for_plot = '.' ;--- path for plots
  IF NOT(KEYWORD_SET(skyl)) THEN skyl = 50                    ;--- percentage of diffuse light
  IF NOT(KEYWORD_SET(Rg))  THEN Rg = 800                      ;--- global radiation (W/m2)

  ;--- physical constants
  h_planck = 6.62606957d-34     ;--- J.s
  c_light = 299792458d0         ;--- m/s
  k_boltzmann = 1.3806488d-23   ;--- J/K
  ;---
  ;cte_stefan = 5.670373d-8      ;--- W/m2/K4
  ;n_avogadro = 6.023e23         ;--- mol-1
  ;f_faraday = 96485.3365        ;--- Coulomb/mol

  ;--- spec of the Sun
  T_sun = 5777d0                ;--- K
  r_Sun = 6.96d8                ;--- radius of Sun in m
  d_Sun_Earth_mean = 1.495d11   ;--- MEAN distance Sun-Earth in m
  ;--- little check
  ;... full spectrum
  ;lm = (dindgen(22000)+10)*1d-9
  ;L_sun_TOA = (2d0*h_planck*c_light^2d0) / (lm^5d) / (EXP((h_planck*c_light)/(lm*k_boltzmann*T_sun)) - 1d)
  ;L_sun_TOA_up = (2d0*h_planck*c_light^2d0) / (lm^5d) * EXP(-(h_planck*c_light)/(lm*k_boltzmann*T_sun))
  ;L_sun_TOA_lo = (2d0*c_light) / (lm^4d) * k_boltzmann*T_sun
  ;print,' --- check unity'
  ;print,' integral of Bl(T)dl = ', TOTAL(L_sun_TOA*1d-9) * !pi * (r_Sun / d_Sun_Earth_mean)^2d0
  ;print,' stefan*Tsun^4       = ',cte_stefan*(T_sun^4) * (r_Sun / d_Sun_Earth_mean)^2d0
  ;... only VIS+NIR
  ;lm = (dindgen(1500)+400)*1d-9
  ;I_sun_TOA = (2d0*h_planck*c_light^2d0) / (lm^5d) / (EXP((h_planck*c_light)/(lm*k_boltzmann*T_sun)) - 1d) $
  ;          * !pi * (r_Sun / d_Sun_Earth_mean)^2d0 ;--- W/m2/m
  ;Rg_Wm2 = total(I_sun_TOA*1e-9)                   ;--- < 1368 W/m2 because over 400-1900nm only
  ;ii_VIS = WHERE(lm GE 400d-9 AND lm LE 700d-9)
  ;PAR_Wm2 = total(I_sun_TOA[ii_VIS]*1e-9) ;--- W/m2
  ;PAR_umolm2s = total(I_sun_TOA[ii_VIS]*1e-9 / (n_avogadro*1e-6*h_planck*c_light/(l[ii_VIS]*1e-9)) )
  ;print,'PAR/Rg = ',PAR_Wm2/Rg_Wm2                ;--- should be 0.48
  ;print,'PAR_umol/Rg_Wm2 = ',PAR_umolm2s/Rg_Wm2   ;--- should be 2.02
  ;print,'PAR_umol/PAR_Wm2 = ',PAR_umolm2s/PAR_Wm2 ;--- should be 2.02*0.48=4.255

  ;--- read sky spectral data
  ;--- note: according to Francois et al. (2002) the
  ;    integral of Es(l).dl and Ed(l).dl over 320nm to 2500nm
  ;    is unity BUT here l runs only from 400nm so it is not
  ;    unity exactly
  data = dataSpec_P5B()
  lamda = REFORM(data(0,*))     ;--- nm
  Es = REFORM(data(7,*))        ;--- um-1
  Ed = REFORM(data(8,*))        ;--- um-1

  ;--- compute field for the plot
  lm = lamda*1d-9
  I_sun_TOA = (2d0*h_planck*c_light^2d0) / (lm^5d) / (EXP((h_planck*c_light)/(lm*k_boltzmann*T_sun)) - 1d) $
              * !pi * (r_Sun / d_Sun_Earth_mean)^2d0 ;--- W/m2/m
  ;---
  Rgdiro = (1.0-skyl/100.0) * Rg * Es[*]*1e6 ;--- W/m2/m
  Rgdifo =      skyl/100.0  * Rg * Ed[*]*1e6 ;--- W/m2/m

  ;--- light spectra to measure ADFC spectral response
;  l2 = [368,388,408,427,447,467,487,506,526,546,$
;        565,585,605,624,644,664,684,703,$
;        723,743,762,782,800,900,1000]*1e-9
;  I2 = [0.0378,0.07472,0.1385,0.2236,0.3696,0.5336,0.6081,0.7017,0.7662,$
;        0.8341,0.9484,1.012,0.9430,0.9995,1.050,1.0773,0.9642,0.9023,$
;        0.8999,0.8646,0.7969,0.6416,0.9837,2.0465,0.7338] * 1e9

  ;--- open ps file
  aspect_ratio=1.1
  xsize=8
  ysize=xsize/aspect_ratio
  set_plot, 'ps'
  !p.font=0
  file = path_for_plot+'solar_spectrum.ps'
  device, filename=file, encapsulated=1, /color, /helvetica
  device, xsize=xsize, ysize=ysize

  ;--- draw a frame
  !p.multi = 0
  plot,[0],[0],$
       charsize=0.8,$ ;1.6,$
       xthick=axis_thickness, ythick=axis_thickness, $
       xminor=1, yminor=1,$
       xrange=[0.2,2.5], yrange=[0,2400], $
;       xrange=[0.4,0.8], yrange=[0,2400], $
       xstyle=1, ystyle=0, $
       xtitle=stdfont+'Wavelength ['+s_micro+stdfont+'m]', $
       ytitle=stdfont+'Solar spectrum [W m!u-2!n '+s_micro+stdfont+'m!u-1!n]', $
       color=0,font=!p.font, $
       /nodata
  
  ;--- plot curves
  oplot,lm*1e6,  I_sun_TOA       *1e-6, color=21, thick=3, linestyle=2
  oplot,lm*1e6, (Rgdiro + Rgdifo)*1e-6, color=2,  thick=3, linestyle=0
  oplot,lm*1e6,  Rgdiro          *1e-6, color=0,  thick=3, linestyle=0
  oplot,lm*1e6,           Rgdifo *1e-6, color=0,  thick=3, linestyle=2
;  oplot,l2*1e6,  I2              *1e-6, color=6,  thick=3, linestyle=1, psym=-6

  ;--- write a legend
  mc_llegend,$
     x=!x.crange[0]+(!x.crange[1]-!x.crange[0])*0.08,$
     y=!y.crange[0]+(!y.crange[1]-!y.crange[0])*0.93, $
     CHARSIZE=0.6,$ ;1.,$
     larr = [1,0,0,1],$ ;[2,0,0,2],$
     ltarr= [1,3,3,3],$
     carr = [21,2,0,0],$
     tarr=stdfont+['TOA at zenith (black body at T!dsun!n=5777K)',$
                   'Total with '+s_phi+stdfont+'!ddiffuse!n='+autostring(skyl/1e2,2)+', R!dg!n='+AUTOSTRING(FIX(Rg))+'W m!u-2!n',$
                   'Direct',$
                   'Diffuse'],$
     row=4,col=1,$
     llength=5,$ ;3,$
     incr_text=2.6,$ ;1.3,$
     vertical=1,cspace=8.,$
     font=!p.font, $
     /data

  ;--- close PS file
  device, /close
  set_plot, 'x'
  !p.font=-1

  ;--- converts to PDF and deletes PS file (for Mac only)
  IF (os NE 'Windows') THEN BEGIN
     cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
     ;cmd = 'ps2pdf '+file+' '+(STR_SEP(file,'.ps'))[0]+'.pdf'
     spawn,cmd
     FILE_DELETE, file
  ENDIF

END

