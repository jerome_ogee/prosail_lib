; PROCEDURE COMMENTS +
; NAME: PRO4SAIL_5B
; AUTHOR: Translated from Matlab to IDL by Eben N. Broadbent 
;  Dept. of Biology, Stanford University.
;  
; Original Matlab code written by: Jean-Baptiste Féret (feret@ipgp.fr)
; Institut de Physique du Globe de Paris, Space and Planetary Geophysics
; during October 2009 based on a version of PROSAIL provided by
; Wout Verhoef, NLR on April/May 2003.
; 
; CONTACT INFO: ebennb@gmail.com
; DESCRIPTION:
; CALLING SEQUENCE:
;       PRO4SAIL5B,Cab,Car,Cbrown,Cw,Cm,Ns,angl,lai,hspot,tts,tto,psi,psoil,skyl,resh,resv
; INPUTS: Cab,Car,Cbrown,Cw,Cm,Ns,angl,lai,hspot,tts,tto,psi,psoil,skyl
; OUTPUTS: resh,resv
; OPTIONAL OUTPUTS:
; OPTIONAL INPUT KEYWORD(S):
; NOTES:
; METHOD:
; EXAMPLE:
; MODIFICATION HISTORY:
;       Downloaded from "http://teledetection.ipgp.jussieu.fr/prosail/" on 09/10/2010
;       Translated from matlab to IDL by: Eben N. Broadbent on '09/27/2010'
;       Changed by: J. Ogee include
;         - Rg (input) and Qabs_l (output) added
;         - re-normalisation of Qdiro and Qdifo according to Rg
;         - variable na added (instead of na=18 only)
; CODING NOTES:
; Same as Matlab (09/24/2010)
;
;********************************************************************************
;*                   SUITS SYSTEM COEFFICIENTS 
;*
;*  ks  : Extinction coefficient for direct solar flux
;*  ko  : Extinction coefficient for direct observed flux
;*  att : Attenuation coefficient for diffuse flux
;*  sigb : Backscattering coefficient of the diffuse downward flux
;*  sigf : Forwardscattering coefficient of the diffuse upward flux
;*  sf  : Scattering coefficient of the direct solar flux for downward diffuse flux
;*  sb  : Scattering coefficient of the direct solar flux for upward diffuse flux
;*  vf   : Scattering coefficient of upward diffuse flux in the observed direction
;*  vb   : Scattering coefficient of downward diffuse flux in the observed direction
;*  w   : Bidirectional scattering coefficient
;********************************************************************************
;-

pro pro4sail_5b,$
   Cab,Car,Cbrown,Cw,Cm,Ns,$
   angl,lai,hspot,tts,tto,psi,psoil,skyl,$
   resh,resv, $
   ;--- input/output ADDED BY JO
   Rg=Rg,$
   l=l, Idir=Idir, Idif=Idif, $
   Qabs_VIS=Qabs_VIS

  ;--- read different spectral data
  ;--- note: according to Francois et al. (2002) the
  ;    integral of Es(l).dl and Ed(l).dl over 320nm to 2500nm
  ;    is unity BUT here l runs only from 400nm so it is not
  ;    unity exactly
  data = dataSpec_P5B()
  Es = data(7,*)                ;--- um-1
  Ed = data(8,*)                ;--- um-1
  Rsoil1 = data(9,*)
  Rsoil2 = data(10,*)

  ;--- LEAF OPTICAL PROPERTIES
  prospect_5B,Cab,Car,Cbrown,Cw,Cm,Ns,l,rho,tau

  ;--- LEAF ANGLE DISTRIBUTION                           
  na     = 13
  thetal = linspace(90/FLOAT(na),90, na) - 45/FLOAT(na)
  lidf   = calc_LIDF(na,angl)

  ;--- DIRECT (Es) and DIFFUSE (Ed) LIGHT
  Qdiro = (1.0-skyl/100.0)*Es                    ;--- um-1
  Qdifo = (    skyl/100.0)*Ed                    ;--- um-1
;;;;;;;;;;;;;;
;  l = REFORM(data(0,*))         ;--- nm
;  c1 = 0.0083581559             ;--- umol/nm
;  Qdiro = Qdiro / (c1 * TOTAL(Es*l)) * Rg
;  Qdifo = Qdifo / (c1 * TOTAL(Ed*l)) * Rg
;;;;;;;;;;;;;;

  ;--- Soil Reflectance Properties                           
  ;--- note: rsoil1 = dry soil                             
  ;          rsoil2 = wet soil                             
  rsoil0=psoil*Rsoil1+(1.0-psoil)*Rsoil2

  ;--- trigonometric variables
  rd     = !pi/180.0
  cts    = cos(rd*tts)
  cto    = cos(rd*tto)
  ctscto = cts*cto
  tants  = tan(rd*tts)
  tanto  = tan(rd*tto)
  cospsi = cos(rd*psi)
  dso    = sqrt(tants*tants+tanto*tanto-2.0*tants*tanto*cospsi)

  ;--- angular distance, compensation of shadow length
  ;    Calculate geometric factors associated with extinction and scattering 
  ;    Initialise sums
  ks  = 0.0
  ko  = 0.0
  bf  = 0.0
  sob = 0.0
  sof = 0.0

  ;---------------------------
  ;--- weighted sums over LIDF
  ;---------------------------
  for i=0, na-1 do begin

     ;---  leaf inclination discrete values
     ttl = thetal(i)
     ctl = cos(rd*ttl)
  
     ;--- SAIL volume scattering phase function gives interception and portions to be 
     ;    multiplied by rho and tau
     ;--- note: for a small subset of values looks good, need more thorough verification
     volscatt,tts,tto,psi,ttl,chi_s,chi_o,frho,ftau
  
     ;--- extinction coefficients
     ksli = chi_s/cts
     koli = chi_o/cto

     ;--- area scattering coefficient fractions
     sobli = frho*!pi/ctscto
     sofli = ftau*!pi/ctscto
     bfli  = ctl*ctl
     ks    = ks+ksli*lidf(i)
     ko    = ko+koli*lidf(i)
     bf    = bf+bfli*lidf(i)
     sob   = sob+sobli*lidf(i)
     sof   = sof+sofli*lidf(i)
  endfor
 
  ;--- geometric factors to be used later with rho and tau
  sdb = 0.5*(ks+bf)
  sdf = 0.5*(ks-bf)
  dob = 0.5*(ko+bf)
  dof = 0.5*(ko-bf)
  ddb = 0.5*(1.0+bf)
  ddf = 0.5*(1.0-bf)

  ;--- here rho and tau come in
  sigb= ddb*rho+ddf*tau
  sigf= ddf*rho+ddb*tau
  att = 1.0-sigf
  m2  =(att+sigb)*(att-sigb)
  ;---
  m2_index = where(m2 le 0.0, m2_count)
  if (m2_count gt 0.0) then m2(m2_index) = 0.0 
  ;---
  m=sqrt(m2)
  ;---
  sb  = sdb*rho+sdf*tau
  sf  = sdf*rho+sdb*tau
  vb  = dob*rho+dof*tau
  vf  = dof*rho+dob*tau
  w = sob*rho+sof*tau

  ;--- outputs for the case LAI = 0
  if (lai lt 0) then begin
     tss    = 1.0
     too    = 1.0
     tsstoo = 1.0
     rdd    = 0.0
     tdd    = 1.0
     rsd    = 0.0
     tsd    = 0.0
     rdo    = 0.0
     tdo    = 0.0
     rso    = 0.0
     rsos   = 0.0
     rsod   = 0.0
     rddt   = rsoil0
     rsdt   = rsoil0
     rdot   = rsoil0
     rsodt  = 0.0
     rsost  = rsoil0
     rsot   = rsoil0
     return
  endif

  ;--- other cases (LAI > 0)
  e1    = exp(-m*lai)
  e2    = e1*e1
  rinf  = (att-m)/sigb
  rinf2 = rinf*rinf
  re    = rinf*e1
  denom = 1.0-rinf2*e2
  ;---
  J1ks=Jfunc1(ks,m,lai)
  J2ks=Jfunc2(ks,m,lai)
  J1ko=Jfunc1(ko,m,lai)
  J2ko=Jfunc2(ko,m,lai)
  ;---
  Ps = (sf+sb*rinf)*J1ks
  Qs = (sf*rinf+sb)*J2ks
  Pv = (vf+vb*rinf)*J1ko
  Qv = (vf*rinf+vb)*J2ko
  ;---
  rdd = rinf*(1.0-e2)/denom
  tdd = (1.0-rinf2)*e1/denom
  tsd = (Ps-re*Qs)/denom
  rsd = (Qs-re*Ps)/denom
  tdo = (Pv-re*Qv)/denom
  rdo = (Qv-re*Pv)/denom
  ;---
  tss = exp(-ks*lai)
  too = exp(-ko*lai)
  z = Jfunc3(ks,ko,lai)
  g1  = (z-J1ks*too)/(ko+m)
  g2  = (z-J1ko*tss)/(ks+m)
  ;---
  Tv1 = (vf*rinf+vb)*g1
  Tv2 = (vf+vb*rinf)*g2
  T1  = Tv1*(sf+sb*rinf)
  T2  = Tv2*(sf*rinf+sb)
  T3  = (rdo*Qs+tdo*Ps)*rinf

  ;--- multiple scattering contribution to bidirectional canopy reflectance
  rsod = (T1+T2-T3)/(1.0-rinf2)

  ;--- treatment of the hotspot-effect
  alf=1e6

  ;... first apply correction 2/(K+k) suggested by F.-M. Bréon
  if hspot gt 0.0 then alf=(dso/hspot)*2.0/(ks+ko)
  if alf gt 200.0 then alf=200.0
  if alf eq 0.0 then begin
  ;... the pure hotspot - no shadow
     tsstoo = tss
     sumint = (1.0-tss)/(ks*lai)
  endif else begin
  ;... outside the hotspot
  ;    Integrate by exponential Simpson method in 20 steps
  ;    the steps are arranged according to equal partitioning
  ;    of the slope of the joint probability function
     fhot=lai*sqrt(ko*ks)
     x1=0.0
     y1=0.0
     f1=1.0
     fint=(1.0-exp(-alf))*0.05
     sumint=0
     for i=1,20 do begin
        if i lt 20 then begin
           x2=-alog(1.0-i*fint)/alf
        endif else begin
           x2=1
        endelse
        y2=-(ko+ks)*lai*x2+fhot*(1.0-exp(-alf*x2))/alf
        f2=exp(y2)
        sumint=sumint+(f2-f1)*(x2-x1)/(y2-y1)
        x1=x2
        y1=y2
        f1=f2
     endfor
     tsstoo=f1
  endelse

  ;--- bidirectional reflectance
  rsos = w*lai*sumint           ;--- Single scattering contribution
  rso=rsos+rsod                 ;--- Total canopy contribution
  dn=1.0-rsoil0*rdd             ;--- Interaction with the soil
  ;---
  rddt=rdd+tdd*rsoil0*tdd/dn
  rsdt=rsd+(tsd+tss)*rsoil0*tdd/dn
  rdot=rdo+tdd*rsoil0*(tdo+too)/dn
  ;---
  rsodt=rsod+((tss+tsd)*tdo+(tsd+tss*rsoil0*rdd)*too)*rsoil0/dn
  rsost=rsos+tsstoo*rsoil0
  rsot=rsost+rsodt
  ;---
  resh  = (rddt*Qdifo+rsdt*Qdiro)/(Qdiro+Qdifo)
  resv  = (rdot*Qdifo+rsot*Qdiro)/(Qdiro+Qdifo)

  ;--- OUTPUT ADDED BY JO
  l = REFORM(data(0,*))         ;--- nm
  Idir = Qdiro*Rg*1e-3          ;--- W/m2/nm
  Idif = Qdifo*Rg*1e-3          ;--- W/m2/nm
  ;--- we need to remove the soil contribution...
  Qabs_l =  ( (1. - rsd - (tsd+tss) + (tsd+tss)*(1.-tdd/dn)*rsoil0) * Idir $
            + (1. - rdd -  tdd      +  tdd     *(1.-tdd/dn)*rsoil0) * Idif) ;-- W/m2/nm
  ;Qabs_l2 =  ( (1. - rsdt - (tsd+tss)*(1.-rsoil0)) * Idir $
  ;           + (1. - rddt -  tdd     *(1.-rsoil0)) * Idif) ;-- W/m2/nm
  ;Qabs_l3 = ((1. - resh)*(Idif+Idir) - (1.-rsoil0)*((tsd+tss)*Idir + tdd*Idif))
  ;print,MAX(Qabs_l-Qabs_l2),MAX(Qabs_l-Qabs_l3)
  ;stop
  ;---
  ii_VIS = WHERE(l GE 400. AND l LE 700.)
  Qabs_VIS = TOTAL(Qabs_l[0,ii_VIS]) ;--- W/m2
end
