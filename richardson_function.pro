PRO Richardson_function, X, a, Y, dYda
;--- a[0] gives baseline value
;--- a[1] gives the (seasonal) amplitude
;--- a[2],a[3] give the timing and rate of increase in rising part
;--- a[4],a[5] give the timing and rate of decrease in falling part
;--- note: when X equals a[2]/a[3] or a[4]/a[5] then Y equals its
;    half value a[0]+a[1]/2

a = FLOAT(a)
F1 = 1./(1.+EXP(a[2]-a[3]*X))
F2 = 1./(1.+EXP(a[4]-a[5]*X))
Y = a[0] + a[1]*F1*F2
;Y = (a[0] + a[1]*F1)*F2
;IF N_PARAMS() GE 4 THEN dYda = [ [replicate(1.,n_elements(X))],$
;                                 [F1*F2],$
;                                 [a[1]*F1*F2*(   EXP(a[2]-a[3]*X))],$
;                                 [a[1]*F1*F2*(-X*EXP(a[2]-a[3]*X))],$
;                                 [a[1]*F1*F2*(   EXP(a[4]-a[5]*X))],$
;                                 [a[1]*F1*F2*(-X*EXP(a[4]-a[5]*X))] ]
END
